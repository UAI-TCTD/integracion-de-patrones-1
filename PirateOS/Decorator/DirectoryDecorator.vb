﻿Public MustInherit Class DirectoryDecorator
    Inherits Directory

    Protected _directory As Directory
    Public Sub New(directory As Directory)
        _directory = directory
    End Sub


    'adapter

    Public Overrides Property Password As String
        Get
            Return _directory.Password
        End Get
        Set(value As String)
            _directory.Password = value
        End Set
    End Property
    Public Overrides ReadOnly Property Path As String
        Get
            Return _directory.Path
        End Get
    End Property

    Public Overrides ReadOnly Property Name As String
        Get
            Return _directory.Name
        End Get
    End Property

    Public Overrides ReadOnly Property Parent As Directory
        Get

            Return _directory.Parent
        End Get
    End Property

    Public Overrides Sub AddComponent(component As FSComponent)
        _directory.AddComponent(component)

    End Sub

    Public Overrides Sub RemoveComponent(component As FSComponent)
        _directory.RemoveComponent(component)
    End Sub

    Public Overrides Function GetChilds() As IReadOnlyCollection(Of FSComponent)
        Return _directory.GetChilds
    End Function

    Public Overrides Function GetFile(name As String) As File
        Return _directory.GetFile(name)
    End Function

    Public Overrides Function GetDirectory(name As String) As Directory
        Return _directory.GetDirectory(name)
    End Function


    Public Overrides ReadOnly Property Size As Double
        Get
            Return _directory.Size
        End Get
    End Property

End Class
