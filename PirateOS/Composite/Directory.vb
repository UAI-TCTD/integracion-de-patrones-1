﻿Public Class Directory
    Inherits FSComponent









    Public Overrides ReadOnly Property Size As Double
        Get
            Dim _size As Double = 0
            For Each component As FSComponent In _components
                _size += component.Size
            Next

            Return _size
        End Get
    End Property


    Public Sub New(name As String, parent As Directory)
        _name = name
        _parent = parent
    End Sub
    Public Sub New()

    End Sub

    Public Overrides Sub AddComponent(component As FSComponent)

        _components.Add(component)
    End Sub

    Public Overrides Sub RemoveComponent(component As FSComponent)
        _components.Remove(component)
    End Sub

    Public Overrides Function GetChilds() As IReadOnlyCollection(Of FSComponent) 'encapsulo el contenido
        Return _components.AsReadOnly()

    End Function

    Public Overrides Function GetDirectory(name As String) As Directory
        Dim component As FSComponent = (_components.Where(Function(c) c.Name.Equals(name)).Where(Function(c) TypeOf c Is Directory).FirstOrDefault())
        If TypeOf component Is Directory Then
            Return DirectCast(component, Directory)
        Else
            Return Nothing
        End If

    End Function



    Public Overrides ReadOnly Property Name As String
        Get

            Return _name

        End Get
    End Property

    Public Overrides Function GetFile(name As String) As File
        Dim component As FSComponent = (_components.Where(Function(c) c.Name.Equals(name)).FirstOrDefault())
        If TypeOf component Is File Then
            Return DirectCast(component, File)
        Else
            Return Nothing
        End If
    End Function




    
End Class
