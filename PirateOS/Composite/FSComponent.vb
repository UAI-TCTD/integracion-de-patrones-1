﻿Public MustInherit Class FSComponent
    Protected _name As String
    
    Protected _components As New List(Of FSComponent)


    Public MustOverride ReadOnly Property Size As Double
    Public MustOverride ReadOnly Property Name As String
    Public MustOverride Function GetChilds() As IReadOnlyCollection(Of FSComponent)
    Public MustOverride Sub AddComponent(component As FSComponent)
    Public MustOverride Sub RemoveComponent(component As FSComponent)
    Public MustOverride Function GetFile(name As String) As File
    Public MustOverride Function GetDirectory(name As String) As Directory

    Overridable Property Password As String

    Protected _parent As Directory

    ReadOnly Property HasPassword As Boolean
        Get
            Return Not (Password Is Nothing OrElse Password.Length = 0)
        End Get
    End Property

    Public Overridable ReadOnly Property Path As String
        Get
            If Not _parent Is Nothing Then
                Return String.Format("{0}/{1}", _parent.Path, _name)
            Else
                Return _name
            End If
        End Get
    End Property

    Public Overridable ReadOnly Property Parent As Directory
        Get
            Return _parent
        End Get
    End Property
        
End Class
