﻿Public Class File
    Inherits FSComponent







    Public _size As Double
    Public Overrides ReadOnly Property Size As Double
        Get
            Return _size
        End Get
    End Property

    Public Sub New(name As String, size As Double)
        _size = size
        _name = name
    End Sub

    Public Overrides ReadOnly Property Name As String
        Get
            Return _name
        End Get
    End Property

    Public Overrides Function GetChilds() As IReadOnlyCollection(Of FSComponent)
        Return Nothing
    End Function



    Public Overrides Sub AddComponent(component As FSComponent)
        Throw New NotImplementedException
    End Sub

    Public Overrides Sub RemoveComponent(component As FSComponent)
        Throw New NotImplementedException
    End Sub

    Public Overrides Function GetDirectory(name As String) As Directory
        Throw New NotImplementedException
    End Function

    Public Overrides Function GetFile(name As String) As File
        Return Me
    End Function
End Class

