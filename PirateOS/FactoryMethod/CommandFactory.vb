﻿Public Class CommandFactory
    Public Shared Function CreateCommand(argument As String) As Command
        Try

            'analizo el comando que se ingresa como parametro
        Dim command() As String
            command = argument.Trim.Split(" ")

            Dim name As String, directoryName As String, param As String = vbNullString
        Dim size As Double = 0

        If command.Length = 0 Or command.Length > 3 Then Throw New InvalidCommandException

            name = command(0)
        

            Select Case name
                Case "cd"
                    directoryName = command(1)
                    If directoryName = ".." Then
                        Return New ReturnDirectoryCommand()
                    Else
                        param = ""
                        If command.Length = 3 Then 'viene con paswword
                            param = command(2)
                        End If
                        Return New ChangeDirectoryCommand(directoryName, param)
                    End If



                Case "md"
                    directoryName = command(1)
                    Return New CreateDirectoryCommand(directoryName)
                Case "ls"
                    Return New ListDirectoryCommand()
                Case "cls"
                    Return New ClearCommand()
                Case ""
                    'no es ningun comando (se apreta enter)
                    Return New MockCommand()
                Case vbNullString
                    'no es ningun comando (string vacio)
                    Return New MockCommand()

                Case "mf"
                    directoryName = command(1)
                    size = command(2)
                    Return New CreateFileCommand(directoryName, size)

                Case "passwd"
                    param = command(1)
                    Return New CreatePasswordCommand(param)
                Case "exit"
                    Return New ExitCommand()
                Case "help"
                    Return New HelpCommand()
                Case Else
                    Throw New InvalidCommandException()
            End Select
        Catch ex As Exception
            Throw New InvalidCommandException()
        End Try

    End Function

End Class
