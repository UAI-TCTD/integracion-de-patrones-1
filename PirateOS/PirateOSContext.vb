﻿Imports PirateOS

Public Class PirateOSContext
    Public Property Online As Boolean
    Dim _actual As Directory
    Dim _fileSystem As List(Of FSComponent)
    Public ReadOnly Property ActualDirectory() As Directory
        Get
            Return _actual
        End Get
    End Property
    Public Sub New()


        'creo el sistema de archivos
        _fileSystem = New List(Of FSComponent)()

        'creo el directorio Raiz
        Dim root As New Directory("root", Nothing)
        _fileSystem.Add(root)
        _actual = root

        'pongo el SO online
        Online = True

    End Sub


    Sub CreatePassword(password As String)


        _actual = New PasswordDecorator(_actual, password)
    End Sub



    Sub ChangeDirectory(dir As Directory)

        _actual = New ChildDecorator(dir)
    End Sub

    Sub ReturnDirectory()
        If _actual.Parent Is Nothing Then
            _actual = _actual
        Else
            _actual = _actual.Parent
        End If

    End Sub

    Private Sub returnToParent(component As FSComponent)

        Dim list As New List(Of FSComponent)(component.GetChilds)

        If list.Contains(_actual) Then
            _actual = component
        Else

            For Each _component As FSComponent In list
                returnToParent(_component)
            Next
        End If


    End Sub



End Class
