﻿Public Class CreateDirectoryCommand
    Inherits Command


    Public Sub New(param As String)
        MyBase.New(param)
    End Sub
    Public Overrides Sub Execute(_context As PirateOSContext)
        If _context.ActualDirectory.GetDirectory(_param) Is Nothing Then
            Dim dir As New Directory(_param, _context.ActualDirectory)
            _context.ActualDirectory.AddComponent(dir)
        Else
            Throw New DirectoryExistException
        End If
    End Sub
End Class
