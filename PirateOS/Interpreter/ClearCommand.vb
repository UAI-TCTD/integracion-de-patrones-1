﻿Public Class ClearCommand
    Inherits Command

    Public Overrides Sub Execute(_context As PirateOSContext)
        Console.Clear()
    End Sub
End Class
