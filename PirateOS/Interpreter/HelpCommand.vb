﻿Public Class HelpCommand
    Inherits Command

    Public Overrides Sub Execute(_context As PirateOSContext)
        Console.WriteLine("Ayuda de PirateOS: ")
        Console.WriteLine(vbTab + "md nombre  (( crea directorio ))")
        Console.WriteLine(vbTab + "mf nombre tamaño (( crea archivo))")
        Console.WriteLine(vbTab + "cd nombre [password] (( accede a directorio))")
        Console.WriteLine(vbTab + "passwd password (( define un password para el directorio actual))")
        Console.WriteLine(vbTab + "ls (( obtiene directorio))")
        Console.WriteLine(vbTab + "cls (( limpia consola ))")
        Console.WriteLine(vbTab + "help  (( esta ayuda))")
        Console.WriteLine(vbTab + "exit (( salir ))")
    End Sub
End Class
