﻿
Class ListDirectoryCommand
    Inherits Command





    Public Overrides Sub Execute(_context As PirateOSContext)

        Console.WriteLine(String.Format("Lista de componentes en {0} ", _context.ActualDirectory.Path))
        Console.Write(vbNewLine)
        For Each component As FSComponent In _context.ActualDirectory.GetChilds

            Console.WriteLine(String.Format("{0} {1} {2} -- Secure: {3}", component.Name, component.Size, component.GetType.Name, component.HasPassword))




        Next
        Console.Write(vbNewLine)
        Console.WriteLine(String.Format("Total de elementos: {0}", _context.ActualDirectory.GetChilds.Count))
        Console.WriteLine(String.Format("Tamaño: {0}", _context.ActualDirectory.Size))

    End Sub
End Class
