﻿Public Class ExitCommand
    Inherits Command

    Public Overrides Sub Execute(_context As PirateOSContext)
        _context.Online = False
        Console.WriteLine("Saliendo de PirateOS...")
    End Sub
End Class
