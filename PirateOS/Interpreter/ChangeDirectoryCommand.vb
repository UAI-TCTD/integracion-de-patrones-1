﻿
Class ChangeDirectoryCommand
    Inherits Command


    Private _password As String

    Sub New(param As String, password As String)
        MyBase.New(param)
        _password = password
    End Sub



    Public Overrides Sub Execute(context As PirateOSContext)
        'me fijo si existe
        Dim dir As Directory = context.ActualDirectory.GetDirectory(_param)
        If Not dir Is Nothing Then

            If dir.HasPassword Then
                If dir.Password.Equals(_password) Then
                    context.ChangeDirectory(dir)
                Else
                    Throw New InvalidPasswordException()
                End If
            Else
                context.ChangeDirectory(dir)
            end if
            Else
                Throw New DirectoryNotFoundException()
            End If
    End Sub
End Class
