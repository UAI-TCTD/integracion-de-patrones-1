﻿
Class CreateFileCommand
    Inherits Command

    Private _name As String
    Private _size As Double

    Sub New(name As String, size As Double)
        ' TODO: Complete member initialization 
        _name = name
        _size = size
    End Sub

    Public Overrides Sub Execute(_context As PirateOSContext)
        Dim file As File
        file = _context.ActualDirectory.GetFile(_name)
        If file Is Nothing Then
            file = New File(_name, _size)
            _context.ActualDirectory.AddComponent(file)
        Else
            Throw New FileExistException()
        End If
    End Sub
End Class
