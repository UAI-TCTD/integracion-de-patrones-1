﻿Public Class CreatePasswordCommand
    Inherits Command
    Dim _password As String
    Public Sub New(password As String)

        _password = password
    End Sub
    Public Overrides Sub Execute(_context As PirateOSContext)
        _context.CreatePassword(_password)
    End Sub
End Class
