﻿Module Module1
    Dim _continue As Boolean = True
    Dim _context As PirateOSContext
    Dim _argument As String
    Dim _command() As String
    Sub Main()
        Console.WriteLine("Iniciando PirateOS...")

        _context = New PirateOSContext()

        Do While _context.Online
            Try
                Console.Write(showActualDirectory)
                _argument = Console.ReadLine()

                Dim command As Command = CommandFactory.CreateCommand(_argument)

                command.Execute(_context)


                
            Catch ex As InvalidCommandException
                Console.WriteLine("Comando incorrecto...")
            Catch ex As InvalidPasswordException
                Console.WriteLine("Password incorrecto...")
            Catch ex As DirectoryNotFoundException
                Console.WriteLine("Directorio no existe...")
            Catch ex As DirectoryExistException
                Console.WriteLine("El directorio existe...")
            Catch ex As FileExistException
                Console.WriteLine("El archivo existe...")
            Catch ex As FileNotFoundException
                Console.WriteLine("El archivo no existe...")
            Catch ex As Exception
                Console.WriteLine("Error desconocido...")
            End Try




        Loop

    End Sub

    Private Function showActualDirectory() As String
        Return String.Format("{0}> ", _context.ActualDirectory.Path)
    End Function


End Module
