﻿Public MustInherit Class Command
    Protected _param As String
    Public Sub New(param As String)
        _param = param
    End Sub

    Public MustOverride Function Execute() As Boolean
End Class
