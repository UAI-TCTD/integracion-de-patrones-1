﻿Public Class CommandFactory
    Public Shared Function CreateCommand(name As String, param As String) As Command
        Select Case name
            Case "cd"
                Return New ChangeDirectoryCommand(param)

            Case Else
                Throw New InvalidCommandException()
        End Select
    End Function

End Class
